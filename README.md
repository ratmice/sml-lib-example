An example of a stabilized library using smlnj/CM.

* contains lib/ with sources.
* bin depends upon lib,
  has a subdirectory bin/lib/ where the top-level lib/ is compiled to without sources.
* mk/ just has some helpful variable extraction from smlnj/CM to make variables.



