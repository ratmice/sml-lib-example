.PHONY: all

all:
	make -C lib install
	make -C bin

clean:
	make -C lib clean 
	make -C bin clean

