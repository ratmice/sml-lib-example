CM_DIR_ARC :=\
$(shell echo \
  'TextIO.output (TextIO.stdErr, CM.cm_dir_arc ^ "\n");' \
  | sml 2>&1 1> /dev/null \
)

OS_KIND :=\
$(shell echo \
'fun kind2name SMLofNJ.SysInfo.BEOS = "beos" | kind2name SMLofNJ.SysInfo.MACOS = "macos" | kind2name SMLofNJ.SysInfo.OS2 = "os2" | kind2name SMLofNJ.SysInfo.UNIX = "unix" | kind2name SMLofNJ.SysInfo.WIN32 = "win32"; val _ = TextIO.output (TextIO.stdErr, ((String.map (Char.toLower) (kind2name(SMLofNJ.SysInfo.getOSKind ()))) ^ "\n"));' \
  | sml 2>&1 1> /dev/null \
)

OS :=\
$(shell echo \
  'TextIO.output (TextIO.stdErr, ((String.map (Char.toLower) (SMLofNJ.SysInfo.getOSName ())) ^ "\n"));' \
  | sml 2>&1 1> /dev/null \
)

ARCH :=\
$(shell echo \
  'TextIO.output (TextIO.stdErr, ((String.map (Char.toLower) (SMLofNJ.SysInfo.getTargetArch ())) ^ "\n"));' \
  | sml 2>&1 1> /dev/null \
)

HEAP_SUFFIX :=\
$(shell echo \
  'TextIO.output (TextIO.stdErr, (SMLofNJ.SysInfo.getHeapSuffix () ^ "\n"));' \
  | sml 2>&1 1> /dev/null \
)

SMLDIR := $(dir $(shell which sml))
